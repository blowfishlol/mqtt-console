const readline = require("readline");
const MqttManager = require("./MqttManager")

const m = new MqttManager("console")
let ready = false


const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const waitInput = async (question) => {
    return new Promise((resolve) =>{
        rl.question(question , (answer) =>{
            resolve(answer)
        })
    })
}



setTimeout(async () =>{
    await m.initialize()
    m.setOnMessageEvent((topic, message) =>{
        console.log()
        console.log(`(RECEIVE) [${topic}] : ${message}`)
    })
    ready = true
    const getCommands = async () =>{
        while(true) {
            let answer = await waitInput("Waiting for input...")
            let tokens = answer.split(" ")
            let command = tokens.shift()
            if(command === "publish") {
                let topic = tokens.shift()
                let message = tokens.join(" ")
                console.log(`(PUBLISH) [${topic}] : [${message}]`)
                m.sendMessage(topic, message)
            }else if (command === "subscribe") {
                let topic = tokens.shift()
                console.log(`Subscribing to topic [${topic}]`)
                m.addSubscription(topic)
            }
        }

    }
    await getCommands()
},0)


rl.on("close", function() {
    console.log("\nThank you.");
    process.exit(0);
});