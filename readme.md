#MQTT Console

###Commands

####Subscribing to topic
```$xslt
subscribe [topic]
```
Example: `subscribe x` 

By doing so, any message with topic `x` will be printed

####Publishing a message
```$xslt
publish [topic] [message]
```

Example: `publish x hi there`

Any programs that subscribed to topic `x` will receive `hi there`