const mqtt = require('mqtt')

class MqttManager {

    //create new instance
    constructor(id) {
        this.mqttClient = null
        this.host = "mqtt://localhost:1883"
        this.id = id
        this.connected = false
        this.onMessageEvent = (topic, message) => {console.log("onMessageEvent triggered. Please fill the callback.")}
    }

    /**
     * Must be executed for the first time after instantiation. Await this.
     * @returns {Promise<unknown>}
     */
    initialize() {
        return new Promise((resolve, reject) => {

            this.mqttClient = mqtt.connect(this.host)

            this.mqttClient.on('error', (err) => {
                console.error(`[${this.id}] Error.`)
                console.error(err)
                reject({message: "An error occured", err: err});
            })

            //listen to connect event. if OK, send resolve
            this.mqttClient.on('connect', () => {
                console.log(`[${this.id}]  Successfully established connection with broker ${this.host}`)
                this.connected = true
                resolve(true)
            })

            //event listener
            this.mqttClient.on('close', () => {
                console.log(`[${this.id}] disconnected.`);
                this.connected = false
            });

            //event listener
            this.mqttClient.on('message', (topic, message) =>{
                message = message.toString()
                this.onMessageEvent(topic, message)
            })

            //set timeout to 5000ms.
            setTimeout(() => {
                reject({message: "Timeout reached.", err: null})
            },5000)

        })
    }

    /**
     * To set on message event
     * @param function fnc
     */
    setOnMessageEvent(fnc) {
        this.onMessageEvent = fnc
    }

    /**
     * To add a new subscription
     * @param sub
     */
    addSubscription(sub) {
        this.mqttClient.subscribe(sub, {qos: 0})
    }

    /**
     * To send message
     * @param topic
     * @param message
     */
    sendMessage(topic, message) {
        if(!this.connected) {
            console.error(`[${this.id}] not ready to publish.`)
        } else {
            this.mqttClient.publish(topic, message)
        }
    }
}

module.exports = MqttManager